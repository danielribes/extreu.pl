#!/usr/bin/perl
#
# 26/09/2012 Daniel Ribes.com
#
# Script per resoldre una tasca de L'O
# 
# - D'un fitxer CSV separat per tabulats, en fa dos fitxers segons contingut
# de la columna 4 (comptades de 0 a n columnes)
#

# - Utilitzat inicialment per separar, segons idioma, files d'un fitxer base
#

$fitxer = 'fitxerbase.csv';
$fitxerCAT ='sortida_CAT.csv';
$fitxerES = 'sortida_ES.csv';

open(FCAT, "> $fitxerCAT");
open(FES, "> $fitxerES");
open(FH, "< $fitxer");
@linies = <FH>;
close FH;

while ($fila = pop @linies) {
    @registre = split(/\t/, $fila);
    if( $registre[4] ne '' ) {
        # catala
        print FCAT join("\t", @registre);
    }  else {
        # espanyol
        print FES join("\t", @registre);  
    }
}

close FCAT;
close FES;
